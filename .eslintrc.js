module.exports = {
  root: true,
  parser: "vue-eslint-parser",
  env: {
    node: true,
    browser: true,
  },
  extends: [
    "@nuxtjs/eslint-config-typescript",
    "plugin:vue/recommended",
    "plugin:vuetify/recommended",
    "eslint:recommended",
    "plugin:prettier/recommended",
  ],
  rules: {
    "linebreak-style": "off",
    "vue/component-name-in-template-casing": ["error", "PascalCase"],
    "vue/multi-word-component-names": "off",
    "vue/require-default-prop": "off",
    // TODO: enable when a workaround has been implemented
    "vue/no-mutating-props": "off",
    "vue/valid-v-slot": "off",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
  },
  overrides: [
    {
      files: ["*.ts"],
      rules: {
        "no-undef": "off",
      },
    },
  ],
  globals: {
    $nuxt: true,
  },
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  ignorePatterns: ["SaxonJS2.js", "xmllint.js", "src/static/xslt-dcc"],
};
