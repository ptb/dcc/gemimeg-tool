import pkg from "./package";

const env = process.env.NODE_ENV || "development";
const isDev = env === "development" || env === "dev";
const defaults = {
  name: "GEMIMEG-Tool",
  description: pkg.description,
  rootDir: null,
  srcDir: "src/",
  theme: {
    colors: {
      primary: "#057EB6",
      secondary: "#424242",
      accent: "#82B1FF",
      error: "#DA3B39",
      info: "#1C54B1",
      success: "#118A0F",
      warning: "#FFC107",
    },
    isDark: false,
    isNavDark: true,
  },
  ssr: false,
  workbox: true,
  target: "static",
  modern: isDev ? false : "client",
  relative: false,
  base: "/",
  plugins: [],
};

export function generateConfig(settings) {
  settings = Object.assign(defaults, settings);
  let pwaSettings = {
    // fix for https://github.com/nuxt-community/pwa-module/issues/509
    workbox: {
      workboxURL: `/workbox/workbox-sw.js?${Date.now()}`,
      config: {
        modulePathPrefix: "/workbox",
      },
    },
  };

  if (settings.relative) {
    pwaSettings = {
      ...pwaSettings,
      meta: {
        favicon: false,
      },
      icon: {
        publicPath: "",
      },
      workbox: {
        publicPath: "_nuxt",
        workboxURL: `workbox/workbox-sw.js?${Date.now()}`,
        config: {
          modulePathPrefix: "workbox",
        },
      },
    };
  }

  return {
    rootDir: settings.rootDir,
    srcDir: settings.srcDir,
    target: settings.target,
    ssr: settings.ssr,
    modern: settings.modern,
    env: {
      version: pkg.version,
      settings,
    },
    telemetry: false,

    babel: {
      presets: ["@nuxt/babel-preset-app"],
    },

    router: settings.relative
      ? {
          mode: "hash",
          base: "./",
        }
      : null,

    // region Headers of the page

    head: {
      title: settings.name,
      link: [
        { rel: "stylesheet", href: `${settings.base}fonts/fonts.css` },
        {
          rel: "stylesheet",
          href: `${settings.base}fonts/mathlive-fonts.css`,
        },
        {
          rel: "apple-touch-icon",
          sizes: "180x180",
          href: `${settings.base}icons/apple-touch-icon.png`,
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: `${settings.base}icons/favicon-32x32.png`,
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: `${settings.base}icons/favicon-16x16.png`,
        },
        {
          rel: "mask-icon",
          color: "#009bce",
          href: `${settings.base}icons/safari-pinned-tab.svg`,
        },
      ],
      script: [{ src: `${settings.base}xmllint.js`, defer: true }],
    },

    // endregion

    // region Progressbar Options

    loading: {
      color: settings.theme.colors.primary,
    },
    loadingIndicator: {
      name: "~/ptb.html",
      background: "#303030",
    },

    // endregion

    /*
     ** Global CSS
     */
    css: ["~/assets/style/app.scss", "@mdi/font/css/materialdesignicons.css"],

    // region Plugins

    plugins: [
      "~/plugins/logger.ts",
      "~/plugins/global.ts",
      "~/plugins/tool.ts",
      "~/plugins/notifier.ts",
      "~/plugins/filters.ts",
      "~/plugins/gLocalizedRoutingMixin.ts",
      "~/plugins/localization.ts",
      "~/plugins/vue-mathlive.js",
      ...settings.plugins,
    ],

    // endregion

    // region Modules

    modules: [
      [
        "@nuxtjs/i18n",
        {
          locales: [
            {
              code: "de",
              name: "Deutsch",
              iso: "de-DE",
              file: "de-DE.json",
            },
            {
              code: "en",
              name: "English",
              iso: "en-US",
              file: "en-US.json",
            },
          ],
          lazy: true,
          langDir: "assets/lang/",
          defaultLocale: "en",
          detectBrowserLanguage: false,
          vueI18n: {
            fallbackLocale: "en",
          },
        },
      ],
      "vue-warehouse/nuxt",
      "@nuxtjs/toast",
    ],

    buildModules: [
      "@nuxt/typescript-build",
      "@nuxtjs/pwa",
      [
        "@nuxtjs/vuetify",
        {
          defaultAssets: false,
        },
      ],
      [
        "@nuxtjs/color-mode",
        {
          preference: "light",
          fallback: "light",
        },
      ],
    ],

    // endregion

    /*
     ** Vuetify options
     */

    vuetify: {
      materialIcons: false,
      theme: {
        themes: {
          light: settings.theme.colors,
        },
        options: { customProperties: true },
      },
      treeShake: true,
    },

    toast: {
      duration: 5000,
      iconPack: "mdi",
    },

    pwa: {
      manifest: {
        name: settings.name,
        short_name: "GT",
        description: settings.description,
        theme_color: settings.theme.colors.primary,
        display: "standalone",
        orientation: "landscape-primary",
      },
      ...pwaSettings,
      workbox: settings.workbox ? pwaSettings.workbox : false,
    },

    publicRuntimeConfig: {
      logLevel: process.env.LOG_LEVEL ?? isDev ? "debug" : "fatal",
      copyrightYear: new Date().getFullYear(),
    },

    build: {
      babel: {
        compact: true,
      },
      extend(config) {
        if (isDev) {
          config.devtool = "eval-source-map";
        }

        config.module.rules.push({
          test: /\.(xsd|xml)$/,
          loader: "raw-loader",
          exclude: /(node_modules)/,
        });
        config.resolve.alias.vue = "vue/dist/vue.common";
      },
      transpile: ["vue-mathlive"],
    },
  };
}
