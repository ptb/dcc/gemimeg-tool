FROM node:20-alpine

RUN apk update && apk upgrade

# create workdir
RUN mkdir -p /usr/src/gemimeg-tool
WORKDIR /usr/src/gemimeg-tool

# copy the tool into the workdir, install deps and build
COPY . /usr/src/gemimeg-tool/
RUN yarn
RUN yarn run attribution:generate
RUN yarn run build

EXPOSE 3000

ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 3000

CMD [ "yarn", "run", "start" ]
