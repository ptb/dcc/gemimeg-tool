import { generateConfig } from "./nuxt.config.generator";

export default generateConfig({
  base: "",
  modern: false,
  ssr: false,
  relative: true,
});
