// run https://www.npmjs.com/package/oss-attribution-generator before this script

import fs from "fs";
import path, { dirname } from "path";
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const loadJSON = (path) => {
  try {
    return JSON.parse(fs.readFileSync(path));
  } catch (e) {
    console.error(e);
    console.error("Did you run oss-attribution-generator before this script?");
    console.error("> npx --yes oss-attribution-generator");
    console.error("Or use the yarn command that does it automatically:");
    console.error("> yarn attribution:generate");
    process.exit(1);
  }
}

const credits = loadJSON(path.join(__dirname, "..", "oss-attribution", "licenseInfos.json"));
const cleaned = Object.values(credits).map((x) => ({
  name: x.name,
  version: x.version,
  authors: x.authors,
  url: x.url,
  license: x.license,
}));

const data = JSON.stringify(cleaned);
fs.writeFileSync(
  path.join(__dirname, "..", "src", "assets", "credits.json"),
  data
);
