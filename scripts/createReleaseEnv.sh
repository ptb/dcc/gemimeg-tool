#!/usr/bin/env bash

mapfile -t TAGS < <(git tag --sort=creatordate --list "v*") # Read tags as array
CURRENT_VER=${TAGS[-2]}
NEXT_VER=${TAGS[-1]}
CURR_DATE=$(date +%F)
COMPARE_URL="${CI_PROJECT_URL}/-/compare/${CURRENT_VER}...${NEXT_VER}"

echo "NEXT_VER=${NEXT_VER}"
echo "FILE_URL=${CI_PROJECT_URL}/-/jobs/artifacts/${NEXT_VER}/download?job=nuxt"
echo "# [${NEXT_VER}](${COMPARE_URL}) (${CURR_DATE})

## This Version supports v3 of the DCC

## Release Notes" > release.description
