import * as fs from "fs";
import * as path from "path";
import { test, expect } from "@playwright/test";

let xml = fs.readFileSync(
  path.join(
    __dirname,
    "..",
    "src",
    "assets",
    "xml",
    "gp",
    "GP_Temperature.xml"
  ),
  "utf8"
);
xml = xml.trimEnd();

test("test", async ({ context, page }) => {
  await page.goto("http://localhost:3000/#/");
  await context.grantPermissions(["clipboard-read", "clipboard-write"]);
  // Load GP Temperature
  await page.locator("button:has-text('Temperature')").click();

  // Open the preview tab
  await page.locator("text=5 Preview").click();

  // Switch to xml
  await page.locator("div[role='tab']:has-text('XML')").click();

  // Copy xml to clipboard
  await page.locator("button .mdi-clipboard-outline").click();
  expect(await page.evaluate(() => navigator.clipboard.readText())).toBe(xml);
});
