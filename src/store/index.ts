import { MutationTree } from "vuex";

export const state = () => ({
  currentUsedLangISO639: "de",
});

export type RootState = ReturnType<typeof state>;

export const mutations: MutationTree<RootState> = {
  setCurrentUsedLangISO639(context, newCurrentUsedLangISO639) {
    context.currentUsedLangISO639 = newCurrentUsedLangISO639;
  },
};
