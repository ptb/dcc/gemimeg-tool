import { MutationTree } from "vuex";
import { lightTheme } from "~/components/Mixins/theme";

export const state = () => ({
  ...lightTheme,
  listMode: true,
});

export type ThemeState = ReturnType<typeof state>;

export const mutations: MutationTree<ThemeState> = {
  setTheme(state, { key, colors, isDark, isNavDark }) {
    state.key = key;
    state.colors = colors;
    state.isDark = isDark;
    state.isNavDark = isNavDark;
  },
  setListMode(state, val) {
    state.listMode = val;
  },
};
