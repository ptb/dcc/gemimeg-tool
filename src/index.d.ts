declare module "@types/nuxt" {
  import { IGlobal } from "~/plugins/global";
  import { IApplication } from "~/core/interfaces/IApplication";
  import { Logger } from "~/plugins/logger";
  import { Notifier } from "~/plugins/notifier";

  interface Context {
    $global: IGlobal;
    $tool: IApplication;
    $logger: Logger;
    $notifier: Notifier;
  }
}

declare module "*.xsd" {
  const content: string;
  export default content;
}

declare module "*.xml" {
  const content: string;
  export default content;
}
