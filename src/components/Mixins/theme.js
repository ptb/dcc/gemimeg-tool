import merge from "lodash/merge";

export const lightTheme = process.env.settings.theme;

export const themeMixin = {
  computed: {
    themeName() {
      return this.$colorMode.preference;
    },
    systemThemeName() {
      return this.$colorMode.value;
    },
    theme() {
      return this.$store.state.theme;
    },
    isDark() {
      return this.$store.state.theme.isDark;
    },
    isNavDark() {
      return this.$store.state.theme.isNavDark;
    },
  },
  watch: {
    systemThemeName() {
      if (this.themeName === "system") {
        this.updateTheme();
      }
    },
    themeName(themeName) {
      this.updateTheme(themeName);
    },
  },
  methods: {
    updateTheme(themeName) {
      if (themeName === "system") themeName = this.$colorMode.value;
      themeName = `${themeName}Theme`;
      if (Object.prototype.hasOwnProperty.call(availableThemes, themeName)) {
        const theme = merge({}, lightTheme, availableThemes[themeName]);
        this.setVuexTheme(theme);
        this.setVuetifyTheme(theme);
      } else {
        this.$logger.error("Theme not found.");
      }
    },
    updateListMode() {
      this.$store.commit(
        "theme/setListMode",
        this.$warehouse.get("listMode", true)
      );
    },
    setTheme(themeName) {
      this.$colorMode.preference = themeName;
    },
    setVuetifyTheme(theme) {
      this.$vuetify.theme.dark = theme.isDark;
      if (theme.isDark) {
        this.$vuetify.theme.themes.dark = theme.colors;
      } else {
        this.$vuetify.theme.themes.light = theme.colors;
      }
    },
    setVuexTheme(theme) {
      this.$store.commit("theme/setTheme", theme);
    },
  },
};

export const darkTheme = {
  isDark: true,
  isNavDark: true,
};

export const availableThemes = {
  systemTheme: {},
  lightTheme,
  darkTheme,
};
