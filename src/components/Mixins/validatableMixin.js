import validatable from "vuetify/es5/mixins/validatable/index";
import mixins from "vuetify/es5/util/mixins";

/*
(ab)using the vuetify validatable mixin for custom validated components

How to use this Mixin

export default {
  name: "MyComponentThatShouldBeValidated",
  computed: {
    customRules() {
      // define your custom rules here
      return [() => this.value.item.length > 0 || this.$t("dcc.items.empty")];
    },
  },
  watch: {
    // the validation has to be triggered when one of the used properties changes
    "value.item"() {
      this.validate();
    },
  },
}
*/

export const validatableMixin = mixins(validatable).extend({
  methods: {
    validate(force = false, value) {
      if (!this.customRules || !Array.isArray(this.customRules)) {
        this.$logger.error("customRules must be defined and of the type array");
      }

      const errorBucket = [];
      value = value || this.internalValue;

      if (force) this.hasInput = this.hasFocused = true;

      for (let index = 0; index < this.customRules.length; index++) {
        const rule = this.customRules[index];
        const valid = typeof rule === "function" ? rule(value) : rule;

        if (valid === false || typeof valid === "string") {
          errorBucket.push(valid || "");
        } else if (typeof valid !== "boolean") {
          this.$logger.error(
            `Rules should return a string or boolean, received '${typeof valid}' instead`,
            this
          );
        }
      }

      this.errorBucket = errorBucket;
      this.valid = errorBucket.length === 0;

      return this.valid;
    },
  },
});
