export const localizationMixin = {
  methods: {
    getForCurrentLocale(item, stringWhenEmpty = "-") {
      if (!item) return stringWhenEmpty;

      let val;
      if (item.content) {
        val = item.getContent(this.$store.state.currentUsedLangISO639);
      }
      return val && val._text ? val._text : stringWhenEmpty;
    },
  },
};
