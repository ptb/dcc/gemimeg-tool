import Vue from "vue";
import { VCol } from "vuetify/lib/components";
const breakpoints = ["cols", "sm", "md", "lg", "xl"];

export default Vue.extend({
  name: "GCol",
  functional: true,
  props: {
    // generate props for all breakpoints
    ...breakpoints.reduce((props, val) => {
      props[val] = {
        type: [Boolean, String, Number],
        default: false,
      };
      return props;
    }, {}),
  },
  render(h, context) {
    // check if listMode is enabled
    context.data.attrs = {
      ...context.props,
    };
    if (context.parent.$store.state.theme.listMode) {
      context.data.class = "listMode";
      context.data.attrs.cols = 12;
      context.data.attrs.sm = 12;
      context.data.attrs.md = 12;
      context.data.attrs.lg = 12;
      context.data.attrs.xl = 12;
    }
    return h(VCol, context.data, context.children);
  },
});
