import {
  DCCXMLElement,
  DCCXMLBoolean,
  SIXMLElement,
  SIXMLBoolean,
} from "@d-ptb/dcc-js";
import { getRules } from "../../Validation";
import { generateLabel } from "../LabelSlotGenerator";
import { generateInfoBtn } from "~/components/App/Form/InfoBtnGenerator";

export const defaultProps = {
  value: Object,
  label: String,
  i18n: String,
  validate: String,
  ns: {
    type: String,
    default: "dcc",
  },
  wikiEntry: String,
};

export function getCtor(ns) {
  switch (ns) {
    case "dcc":
      return {
        String: DCCXMLElement,
        Boolean: DCCXMLBoolean,
      };
    case "si":
      return {
        String: SIXMLElement,
        Boolean: SIXMLBoolean,
      };
  }
}

export class XmlInput {
  static render(element, h, context, isCheckBox = false) {
    const ctor = getCtor(context.props.ns);

    const label = context.props.i18n
      ? context.parent.$t(context.props.i18n)
      : context.props.label;

    context.data.attrs.rules = [
      ...(context.data.attrs.rules || []),
      ...getRules(context.parent, label, context.props.validate),
    ];

    const item = context.props.value;
    let value;

    if (item) {
      if (Object.prototype.hasOwnProperty.call(item, "_text")) {
        value = item._text;
      } else if (Object.prototype.hasOwnProperty.call(item, "value")) {
        value = item.value;
      } else {
        console.warn(
          "Please use a object with a _text or value field.",
          item,
          context
        );
      }
    }

    const cb = (val) => {
      if (val) {
        if (item) {
          item._text = val;
          context.data.model.callback(item);
        } else {
          context.data.model.callback(
            isCheckBox
              ? new ctor.Boolean({ _text: val })
              : new ctor.String({ _text: val })
          );
        }
      } else {
        context.data.model.callback(undefined);
      }
    };

    return h(
      element,
      {
        attrs: context.data.attrs,
        props: {
          value,
        },
        on: {
          ...context.data.on,
          input: cb,
        },
        model: {
          ...context.data.model,
          callback: cb,
          value,
        },
      },
      [
        generateLabel(
          h,
          label,
          context.props.validate && context.props.validate.includes("required")
        ),
        generateInfoBtn(h, context.props.wikiEntry),
      ]
    );
  }
}
