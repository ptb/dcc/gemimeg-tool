import VCheckbox from "vuetify/es5/components/VCheckbox/VCheckbox";
import { XmlInput, defaultProps } from "./XmlInput";

export default {
  name: "XmlCheckbox",
  functional: true,
  props: defaultProps,
  render(h, context) {
    return XmlInput.render(VCheckbox, h, context, true);
  },
};
