import { StringWithLangType } from "@d-ptb/dcc-js";
import { getRules } from "../../Validation";
import { generateLabel } from "../LabelSlotGenerator";
import { generateInfoBtn } from "~/components/App/Form/InfoBtnGenerator";
import { cleanUpContentArray } from "~/assets/util/dccUtil";

export class DccLocalizedInput {
  static render(element, h, context, ElemCtor) {
    const label = context.props.i18n
      ? context.parent.$t(context.props.i18n)
      : context.props.label;

    let textBlock = context.props.value;

    if (!textBlock || typeof textBlock.getContent !== "function") {
      textBlock = textBlock ? new ElemCtor(textBlock) : new ElemCtor();
    }

    const usedLangs =
      context.parent.$store.$global.dccDocument.digitalCalibrationCertificate
        .administrativeData.coreData.usedLangCodeISO639_1;

    const inputs = [];

    for (let i = 0; i < usedLangs.length; i++) {
      context.data.attrs.rules = [
        ...(context.data.attrs.rules || []),
        ...getRules(context.parent, label, context.props.validate),
      ];

      let item = textBlock.getContent(usedLangs[i]._text);

      const updateContent = (val) => {
        if (item) {
          item._text = val;
        } else {
          item = new StringWithLangType({
            _text: "",
            _attr: { lang: usedLangs[i]._text },
          });
          textBlock.content.push(item);
        }

        if (textBlock && !val.trim()) {
          cleanUpContentArray(textBlock.content);

          if (textBlock.content.length <= 0) {
            textBlock = undefined;
          }
        }
      };

      const value = item?._text ?? "";

      const elt = h(
        element,
        {
          attrs: context.data.attrs,
          props: {
            value,
            autoGrow: true,
            rows: 1,
          },
          on: {
            ...context.data.on,
            input(val) {
              updateContent(val);
              context.data.on.input(textBlock);
            },
          },
          model: {
            ...context.data.model,
            callback(val) {
              updateContent(val);
              context.data.model.callback(textBlock);
            },
            value,
          },
        },
        [
          generateLabel(
            h,
            "",
            context.props.validate &&
              context.props.validate.includes("required"),
            `${usedLangs[i]._text}`
          ),
          generateInfoBtn(h, context.props.wikiEntry),
        ]
      );

      inputs.push(elt);
    }
    return h(
      "div",
      {
        class: "px-4 py-2",
        style: "background-color: #f5f5f5; border-radius: 5px",
      },
      [h("span", { style: "font-size: 16px; color: black" }, label), ...inputs]
    );
  }
}
