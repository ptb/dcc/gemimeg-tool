import VTextarea from "vuetify/lib/components/VTextarea/VTextarea";
import { RichContentType, TextType } from "@d-ptb/dcc-js";
import { DccLocalizedInput } from "./DccLocalizedInput";

export default {
  name: "DccLocalizedTextarea",
  functional: true,
  props: {
    value: Object,
    label: String,
    i18n: String,
    validate: String,
    richContent: Boolean,
    wikiEntry: String,
    autoGrow: Boolean,
  },
  render(h, context) {
    return DccLocalizedInput.render(
      VTextarea,
      h,
      context,
      context.props.richContent ? RichContentType : TextType
    );
  },
};
