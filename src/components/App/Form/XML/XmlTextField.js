import VTextField from "vuetify/es5/components/VTextField/VTextField";
import { XmlInput, defaultProps } from "./XmlInput";

export default {
  name: "XmlTextField",
  functional: true,
  props: defaultProps,
  render(h, context) {
    return XmlInput.render(VTextField, h, context);
  },
};
