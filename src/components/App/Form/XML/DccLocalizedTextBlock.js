import VTextField from "vuetify/lib/components/VTextField/VTextField";
import { RichContentType, TextType } from "@d-ptb/dcc-js";
import { DccLocalizedInput } from "./DccLocalizedInput";

export default {
  name: "DccLocalizedTextBlock",
  functional: true,
  props: {
    value: Object,
    label: String,
    i18n: String,
    validate: String,
    richContent: Boolean,
    wikiEntry: String,
  },
  render(h, context) {
    return DccLocalizedInput.render(
      VTextField,
      h,
      context,
      context.props.richContent ? RichContentType : TextType
    );
  },
};
