import VTextarea from "vuetify/es5/components/VTextarea/VTextarea";
import { XmlInput, defaultProps } from "./XmlInput";

export default {
  name: "XmlTextArea",
  functional: true,
  props: defaultProps,
  render(h, context) {
    return XmlInput.render(VTextarea, h, context);
  },
};
