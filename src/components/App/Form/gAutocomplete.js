import VAutocomplete from "vuetify/es5/components/VAutocomplete/VAutocomplete";
import { gInput } from "./gInput";

export default {
  name: "gAutocomplete",
  functional: true,
  props: ["value", "label", "i18n", "validate", "wikiEntry"],
  render(h, context) {
    return gInput.render(VAutocomplete, h, context);
  },
};
