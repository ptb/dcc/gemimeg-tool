import { generateWikiUri } from "~/assets/util/general";
import GIconBtn from "~/components/App/Common/GIconBtn";

export function generateInfoBtn(h, wikiEntry) {
  if (wikiEntry) {
    const btn = h(
      GIconBtn,
      {
        on: {
          click: () => {
            window.open(generateWikiUri(wikiEntry), "_blank").focus();
          },
        },
        props: {
          i18n: "form.showInformation",
          icon: "mdi-information",
          color: "grey darken-1",
          small: true,
          smallBtn: true,
        },
      },
      []
    );
    return h("span", { slot: "append-outer" }, [btn]);
  }
}
