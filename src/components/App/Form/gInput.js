import { getRules } from "../Validation";
import { generateLabel } from "./LabelSlotGenerator";
import { generateInfoBtn } from "~/components/App/Form/InfoBtnGenerator";

export class gInput {
  static render(element, h, context, attrRefType) {
    const label = context.props.i18n
      ? context.parent.$t(context.props.i18n)
      : context.props.label;

    context.data.attrs.rules = [
      ...(context.data.attrs.rules || []),
      ...getRules(context.parent, label, context.props.validate),
    ];

    if (attrRefType) {
      let value = context.props.value;
      const cb = (val) => {
        if (val) {
          value = val;
          context.data.model.callback(value);
        } else {
          context.data.model.callback(undefined);
        }
      };

      return h(
        element,
        {
          attrs: context.data.attrs,
          props: {
            value,
          },
          on: {
            ...context.data.on,
            input: cb,
          },
          model: {
            ...context.data.model,
            callback: cb,
            value,
          },
        },
        [
          generateLabel(
            h,
            label,
            context.props.validate &&
              context.props.validate.includes("required")
          ),
          generateInfoBtn(h, context.props.wikiEntry),
        ]
      );
    } else {
      return h(element, context.data, [
        generateLabel(
          h,
          label,
          context.props.validate && context.props.validate.includes("required")
        ),
        generateInfoBtn(h, context.props.wikiEntry),
      ]);
    }
  }
}
