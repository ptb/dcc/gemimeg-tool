import VTextarea from "vuetify/es5/components/VTextarea/VTextarea";
import { gInput } from "./gInput";

export default {
  name: "gTextArea",
  functional: true,
  props: ["value", "label", "i18n", "validate"],
  render(h, context) {
    return gInput.render(VTextarea, h, context);
  },
};
