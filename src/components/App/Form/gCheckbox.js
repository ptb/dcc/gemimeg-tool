import VCheckbox from "vuetify/es5/components/VCheckbox/VCheckbox";
import { gInput } from "./gInput";

export default {
  name: "gCheckbox",
  functional: true,
  props: ["value", "label", "i18n", "validate", "wikiEntry"],
  render(h, context) {
    return gInput.render(VCheckbox, h, context);
  },
};
