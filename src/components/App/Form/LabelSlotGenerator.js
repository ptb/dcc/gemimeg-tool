export function generateLabel(h, label, required, extraText) {
  const children = [];

  if (extraText) {
    children.push(
      h(
        "span",
        {
          class: "font-weight-thin",
        },
        [` ${extraText}`]
      )
    );
  }

  if (required) {
    // if element is required add red star
    children.push(
      h(
        "span",
        {
          class: "red--text text--darken-2",
        },
        [" *"]
      )
    );
  }

  // create slot for label
  return h(
    "span",
    {
      slot: "label",
    },
    [label, ...children]
  );
}
