import VTextField from "vuetify/es5/components/VTextField/VTextField";
import { gInput } from "./gInput";

export default {
  name: "gTextField",
  functional: true,
  props: {
    value: String,
    label: String,
    i18n: String,
    validate: String,
    attrRefType: Boolean,
  },
  render(h, context) {
    return gInput.render(VTextField, h, context, context.props.attrRefType);
  },
};
