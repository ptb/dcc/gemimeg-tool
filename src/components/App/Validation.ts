import {
  BETWEEN_ZERO_AND_ONE_INCLUSIVE_REGEX,
  DECIMAL_LIST_REGEX,
  DECIMAL_REGEX,
  GREATER_OR_EQUAL_ONE_REGEX,
  POSITIVE_DECIMAL_REGEX,
  DATE_TIME_REGEX,
  DATE_TIME_LIST_REGEX,
  BOOLEAN_LIST_REGEX,
  NOT_EMPTY_STRING_TYPE_REGEX,
  DSI_UNIT_REGEX,
  DSI_UNIT_LIST_REGEX,
} from "~/assets/util/constants";

/**
 * Wraps an existing rule to make it optional
 * @param val value to check
 * @param predicate remaining predicate of the existing rule
 */
function optional(val, predicate: boolean) {
  return !val || predicate;
}

export function getRules(parent, label: string, validate: string) {
  // fix for $t this context
  // eslint-disable-next-line no-useless-call
  const $t = (str, arr) => parent.$t.call(parent, str, arr);
  const rules = [];

  if (validate) {
    for (const rule of validate.split(" ")) {
      switch (rule) {
        case "required":
          rules.push((v) => !!v || $t("form.errors.required", [label]));
          break;
        case "notEmptyStringType":
          rules.push((v) =>
            optional(
              v,
              NOT_EMPTY_STRING_TYPE_REGEX.test(v) ||
                $t("form.errors.notEmptyStringType", [label])
            )
          );
          break;
        case "decimal":
          rules.push((v) =>
            optional(
              v,
              DECIMAL_REGEX.test(v) || $t("form.errors.decimal", [label])
            )
          );
          break;
        case "decimalPositive":
          rules.push((v) =>
            optional(
              v,
              POSITIVE_DECIMAL_REGEX.test(v) ||
                $t("form.errors.decimalPositive", [label])
            )
          );
          break;
        case "decimalList":
          rules.push((v) =>
            optional(
              v,
              DECIMAL_LIST_REGEX.test(v) ||
                $t("form.errors.decimalList", [label])
            )
          );
          break;
        case "dsiUnit":
          rules.push((v) =>
            optional(
              v,
              DSI_UNIT_REGEX.test(v) || $t("form.errors.dsiUnit", [label])
            )
          );
          break;
        case "dsiUnitList":
          rules.push((v) =>
            optional(
              v,
              DSI_UNIT_LIST_REGEX.test(v) ||
                $t("form.errors.dsiUnitList", [label])
            )
          );
          break;
        case "dateTime":
          rules.push((v) =>
            optional(
              v,
              DATE_TIME_REGEX.test(v) || $t("form.errors.dateTime", [label])
            )
          );
          break;
        case "dateTimeList":
          rules.push((v) =>
            optional(
              v,
              DATE_TIME_LIST_REGEX.test(v) ||
                $t("form.errors.dateTimeList", [label])
            )
          );
          break;
        case "booleanList":
          rules.push((v) =>
            optional(
              v,
              BOOLEAN_LIST_REGEX.test(v) ||
                $t("form.errors.booleanList", [label])
            )
          );
          break;
        case "kValue":
          rules.push((v) =>
            optional(
              v,
              GREATER_OR_EQUAL_ONE_REGEX.test(v) ||
                $t("form.errors.kValue", [label])
            )
          );
          break;
        case "probabilityValue":
          rules.push((v) =>
            optional(
              v,
              BETWEEN_ZERO_AND_ONE_INCLUSIVE_REGEX.test(v) ||
                $t("form.errors.probabilityValue", [label])
            )
          );
          break;
      }
    }
  }
  return rules;
}
