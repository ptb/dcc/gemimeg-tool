import Vue from "vue";
import { IApplication } from "./interfaces/IApplication";
import { IPreviewStrategy } from "./strategies/preview/IPreviewStrategy";
import { IStorageStrategy } from "./strategies/storage/IStorageStrategy";
import { IValidationStrategy } from "~/core/strategies/validation/IValidationStrategy";
import { ICredit } from "~/core/interfaces/ICredit";

export class GemimegTool implements IApplication {
  $global: Vue;
  previewStrategy: IPreviewStrategy;
  validationStrategy: IValidationStrategy;
  storageStrategy: IStorageStrategy;
  private creditMap: Map<string, ICredit>;

  constructor($global: Vue) {
    this.$global = $global;
    this.creditMap = new Map<string, ICredit>();
  }

  on(name: string, callback: Function): void {
    this.$global.$on(name, callback);
  }

  off(name: string, callback: Function): void {
    this.$global.$off(name, callback);
  }

  emit(name: string, ...args: any[]): void {
    this.$global.$emit(name, ...args);
  }

  getCredits(): ICredit[] {
    return Array.from(this.creditMap.values());
  }

  registerCredits(credits: ICredit | ICredit[]) {
    credits = credits instanceof Array ? credits : [credits];
    for (const credit of credits) {
      if (!this.creditMap.has(credit.name)) {
        this.creditMap.set(credit.name, credit);
      }
    }
  }

  unregisterCredits(credits: ICredit | ICredit[]) {
    credits = credits instanceof Array ? credits : [credits];
    for (const credit of credits) {
      if (this.creditMap.has(credit.name)) {
        this.creditMap.delete(credit.name);
      }
    }
  }
}
