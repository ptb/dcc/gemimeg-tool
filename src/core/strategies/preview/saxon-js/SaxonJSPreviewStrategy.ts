import { DCCDocument } from "@d-ptb/dcc-js";
import { createStore, del, entries, get, update, UseStore } from "idb-keyval";
import { IPreviewStrategy } from "../IPreviewStrategy";
import saxon from "./SaxonJS2";
import { loadFromUrl } from "~/assets/util/general";
import { IApplication } from "~/core/interfaces/IApplication";

const MAX_SEF_CACHE_TIME = 60 * 60 * 24 * 30; // 1 month
const saxonPlatform = saxon.getPlatform();

class SefCache {
  customStore: UseStore;

  constructor() {
    this.customStore = createStore("saxonJsPreview", "sef");
  }

  async cleanup(): Promise<void> {
    const items = await entries(this.customStore);
    for (const [key, val] of items) {
      // check if item is older than MAX_SEF_CACHE_TIME
      if (val.timestamp < Date.now() - MAX_SEF_CACHE_TIME) {
        await this.delete(key.toString());
      }
    }
  }

  async get(id: string): Promise<any> {
    await this.cleanup();
    const item = await get(id, this.customStore);
    return item?.sef;
  }

  async set(id: string, sef: any): Promise<void> {
    await this.cleanup();
    await update(
      id,
      (_) => ({
        sef,
        timestamp: Date.now(),
      }),
      this.customStore
    );
  }

  async delete(id: string): Promise<void> {
    await del(id, this.customStore);
  }
}

export class SaxonJSPreviewStrategy implements IPreviewStrategy {
  xslPath: string;
  base: string;
  sefCache: SefCache;

  constructor($tool: IApplication, xslPath: string, base = "") {
    this.xslPath = xslPath;
    this.base = base;
    this.sefCache = new SefCache();
    $tool.registerCredits({
      name: "saxon-js",
      version: "2.5.0",
      license:
        "Custom: https://www.saxonica.com/saxon-js/documentation2/index.html#!conditions/public-license",
      authors: "Saxonica Ltd",
      url: "https://www.saxonica.com/saxon-js",
    });
  }

  private compileSef(xsl: string) {
    xsl = xsl.replace(
      `<xsl:variable name="base" select="''"/>`,
      `<xsl:variable name="base" select="'${this.base}/'" />`
    );
    const doc = saxonPlatform.parseXmlFromString(xsl);
    doc._saxonBaseUri = "file:///";
    return saxon.compile(doc);
  }

  private async hash(str: string): Promise<string> {
    // https://www.30secondsofcode.org/js/s/hash-browser/
    const h = await crypto.subtle.digest(
      "SHA-256",
      new TextEncoder().encode(str)
    );
    const hexes = [];
    const view = new DataView(h);
    for (let i = 0; i < view.byteLength; i += 4)
      hexes.push(("00000000" + view.getUint32(i).toString(16)).slice(-8));
    return hexes.join("");
  }

  async renderPreview(item: DCCDocument): Promise<string> {
    const xsl = await loadFromUrl(this.xslPath);
    const id = await this.hash(xsl);

    // check if sef is in cache
    let sef = await this.sefCache.get(id);
    if (!sef) {
      sef = await this.compileSef(xsl);
      await this.sefCache.set(id, sef);
    }

    const resultStringXML = saxon.transform({
      stylesheetInternal: sef,
      sourceText: item.toXML(),
      destination: "serialized",
    });

    return resultStringXML.principalResult;
  }
}
