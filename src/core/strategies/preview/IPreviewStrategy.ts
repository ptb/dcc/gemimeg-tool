import { DCCDocument } from "@d-ptb/dcc-js";

export interface IPreviewStrategy {
  renderPreview(item: DCCDocument): Promise<string>;
}
