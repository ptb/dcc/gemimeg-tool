import { DCCDocument } from "@d-ptb/dcc-js";
import { IPreviewStrategy } from "../IPreviewStrategy";
import { loadFromUrl } from "~/assets/util/general";

export class XSLPreviewStrategy implements IPreviewStrategy {
  xslPath: string;
  base: string;

  constructor(xslPath: string, base = "") {
    this.xslPath = xslPath;
    this.base = base;
  }

  async renderPreview(item: DCCDocument): Promise<string> {
    let xsl = await loadFromUrl(this.xslPath);
    xsl = xsl.replace(
      `<xsl:variable name="base" select="''"/>`,
      `<xsl:variable name="base" select="'${this.base}/'" />`
    );

    const parsedXsl = new DOMParser().parseFromString(xsl, "text/xml");
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(parsedXsl);

    const parsedXml = new DOMParser().parseFromString(item.toXML(), "text/xml");
    const resultDocument = xsltProcessor.transformToDocument(parsedXml);

    return Promise.resolve(resultDocument.documentElement.outerHTML);
  }
}
