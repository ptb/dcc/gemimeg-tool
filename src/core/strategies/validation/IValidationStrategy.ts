import { DCCDocument } from "@d-ptb/dcc-js";
import { IValidationResult } from "~/core/strategies/validation/IValidationResult";

export interface IValidationStrategy {
  validate(xml: string): Promise<IValidationResult>;
  validate(dccDocument: DCCDocument): Promise<IValidationResult>;
}
