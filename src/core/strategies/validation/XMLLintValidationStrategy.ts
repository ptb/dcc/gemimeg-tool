import { DCCDocument } from "@d-ptb/dcc-js";
import { IValidationStrategy } from "~/core/strategies/validation/IValidationStrategy";
import { IError } from "~/core/strategies/validation/IError";
import si from "~/assets/xml/schema/si.xsd";
import xmldsig from "~/assets/xml/schema/xmldsig-core-schema.xsd";
import { IValidationResult } from "~/core/strategies/validation/IValidationResult";
import { replaceB64ContentInDOM } from "~/assets/util/general";
import {
  latestSchemaVersion,
  supportedSchemaVersions,
} from "~/assets/util/constants";

interface IXMLLintResult {
  errors: string[];
}

class Error implements IError {
  line: number;
  type: string;
  elementName: string;
  text: string;
  fullText: string;

  toString(): string {
    return `${this.line}:${
      this.elementName ? ` element ${this.elementName}` : ""
    } ${this.type}: ${this.text}${this.fullText ? `\n${this.fullText}` : ""}`;
  }
}

export class XMLLintValidationStrategy implements IValidationStrategy {
  private static async performValidation(xml): Promise<IXMLLintResult> {
    // select the correct schema
    let dccSchemaVersion = new DOMParser()
      .parseFromString(xml, "text/xml")
      .documentElement.getAttribute("schemaVersion");
    if (!supportedSchemaVersions.includes(dccSchemaVersion)) {
      dccSchemaVersion = latestSchemaVersion;
    }
    const dcc = (
      await import(
        `~/assets/xml/schema/dcc${dccSchemaVersion.replace(/\./g, "_")}.xsd`
      )
    ).default;

    // xmllint stores the d-si schema as file_0.xsd. We have to replace the schema location to point at it
    let modifiedDcc = dcc.replace(
      /schemaLocation="https:\/\/ptb\.de\/si\/v\d\.\d\.\d\/SI_Format\.xsd"/,
      `schemaLocation="file_0.xsd"`
    );

    // same applies for xmldsig schema (W3 -> use PTB/ W3 version), it is stored as file_1.xsd
    modifiedDcc = modifiedDcc.replace(
      /schemaLocation="https:\/\/www\.w3\.org\/TR\/xmldsig-core\/xmldsig-core-schema\.xsd"/,
      `schemaLocation="file_1.xsd"`
    );

    // same applies for xmldsig schema (PTB/ W3), it is stored as file_1.xsd
    modifiedDcc = modifiedDcc.replace(
      /schemaLocation="https:\/\/www\.ptb\.de\/dcc\/d-sig\/xmldsig-core-schema\.xsd"/,
      `schemaLocation="file_1.xsd"`
    );

    // TODO: Remove when using wasm version of xmllint
    // Workaround to not validate MathML, because that leads to an OOM error
    modifiedDcc = modifiedDcc.replace(
      /<xs:any namespace="##other" minOccurs="0"\/>/,
      `<xs:any namespace="##other" minOccurs="0" processContents="lax"/>`
    );

    // @ts-ignore
    return window.xmllint.validateXML({
      xml,
      schema: [si, xmldsig, modifiedDcc],
    });
  }

  private static parseErrors(lines: string[]): IError[] {
    const results = [];

    for (const line of lines) {
      /*
      === Example for xmllint output ===

      ## Format of error lines (seperated by colons)
      Part 0     : Filename (always starts with file_)
      Part 1     : Line Number
      Part 2     : Element Name (optional)
      Part 2 or 3: Error Type
      Part 3 or 4: Error Text
      Additional Lines: Full Error Text

      ## 1.) Error with element name
      part 0   | 1 | part 2                      | part 3                 | part 4
      file_0.xml:168: element usedLangCodeISO639_1: Schemas validity error : Element '{https://ptb.de/dcc}usedLangCodeISO639_1': This element is not expected. Expected is ( {https://ptb.de/dcc}countryCodeISO3166_1 ).

      ## 2.) Error without element name
      part 0    | 1 | part 2       | part 3
      file_0.xml:145: parser error : Opening and ending tag mismatch: item line 94 and items
              </dcc:items>
                          ^
      */

      // split line by colon
      const parts = line.split(":");

      // check if the next error starts
      if (parts.length >= 4 && parts[0].startsWith("file_")) {
        const result = new Error();
        result.line = parseInt(parts[1]);

        // extract element if there is one
        if (parts[2].trim().startsWith("element")) {
          result.elementName = parts[2].trim().replace("element ", "");
          result.type = parts[3].trim();
        } else {
          // there is no element. part 2 is the error type
          result.type = parts[2].trim();
        }

        // if there is an element name the text is part 4 otherwise part 3
        result.text = `${parts
          .splice(result.elementName ? 4 : 3)
          .join(":")
          .trim()}`;

        results.push(result);
      } else {
        if (!results.length) {
          results.push({});
        }

        // the line is no standalone error and is an additional line for the last error
        const result = results[results.length - 1];

        // add a new line if there's already text and append error text
        result.fullText = `${
          result.fullText ? `${result.fullText}\n` : ""
        }${line}`;
      }
    }

    return results;
  }

  async validate(item: string | DCCDocument): Promise<IValidationResult> {
    if (typeof item !== "string") {
      item = item.toXML();
    }
    // Filter out files if DCC is larger than 4 MB
    // File attachments are not validated in this case
    if (new Blob([item]).size > 4000000) {
      const domParser = new DOMParser();
      const xmlDom = domParser.parseFromString(item, "text/xml");
      replaceB64ContentInDOM(xmlDom.documentElement);
      const xmlSerializer = new XMLSerializer();
      item = xmlSerializer.serializeToString(xmlDom);
    }

    const result = await XMLLintValidationStrategy.performValidation(item);
    const errors = result.errors
      ? XMLLintValidationStrategy.parseErrors(result.errors)
      : [];
    return Promise.resolve({
      valid: !errors.length,
      errors,
    });
  }
}
