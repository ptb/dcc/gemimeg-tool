import { IError } from "~/core/strategies/validation/IError";

export interface IValidationResult {
  valid: boolean;
  errors: IError[];
}
