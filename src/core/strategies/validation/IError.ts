export interface IError {
  line: number;
  elementName: string;
  type: string;
  text: string;
  fullText: string;
}
