import { ILoadable } from "./interfaces/ILoadable";

export class DirectlyLoadable<T> implements ILoadable<T> {
  loadableElement: T;

  constructor(loadableElement: T) {
    this.loadableElement = loadableElement;
  }

  load(): Promise<T> {
    return Promise.resolve(this.loadableElement);
  }
}
