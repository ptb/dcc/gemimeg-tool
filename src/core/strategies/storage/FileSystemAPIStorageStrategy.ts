import { clear, createStore, del, update, values } from "idb-keyval";
import {
  IDCCFile,
  IStorageStrategy,
} from "~/core/strategies/storage/IStorageStrategy";

// Code based on https://developer.chrome.com/articles/file-system-access/#stored-file-or-directory-handles-and-permissions
async function verifyPermissionOrThrow(fileHandle) {
  const options = {
    mode: "readwrite",
  };
  // Check if permission was already granted
  if ((await fileHandle.queryPermission(options)) === "granted") {
    return;
  }
  // Request permission
  if ((await fileHandle.requestPermission(options)) === "granted") {
    return;
  }
  // The user didn't grant permission, throw an error
  throw new Error("The permission for the file system was not granted.");
}

const customStore = createStore("gemimeg-tool-files", "files");

class FSFile implements IDCCFile {
  autoSave: boolean;
  dateTime: string;
  fileName: string;
  filePath: string;
  id: string;
  handle: FileSystemFileHandle;

  constructor(file: Partial<FSFile>) {
    this.autoSave = file.autoSave;
    this.dateTime = file.dateTime;
    this.fileName = file.fileName;
    this.filePath = file.filePath;
    this.id = file.id;
    this.handle = file.handle;
  }

  async load(): Promise<string> {
    await verifyPermissionOrThrow(this.handle);
    const file = await this.handle.getFile();
    // update metadata in indexeddb
    await update(this.id, (_) => this, customStore);
    return await file.text();
  }

  async save(data: string): Promise<void> {
    const writable = await this.handle.createWritable();
    await writable.write(data);
    await writable.close();

    // update metadata in indexeddb
    this.dateTime = new Date().toISOString();
    await update(this.id, (_) => this, customStore);
  }

  static async fromHandle(handle) {
    await verifyPermissionOrThrow(handle);
    const dccFile = new FSFile({
      autoSave: false,
      handle,
    });
    const file = await handle.getFile();
    dccFile.id = handle.name;
    dccFile.fileName = file.name;
    dccFile.filePath = file.name;
    dccFile.dateTime = file.lastModifiedDate.toISOString();
    return dccFile;
  }
}

const types = [
  {
    description: "DCC XML Files",
    accept: {
      "application/xml": [".xml"],
    },
  },
];

export class FileSystemAPIStorageStrategy implements IStorageStrategy {
  async addLastLoadedItem(item: IDCCFile): Promise<void> {
    await update(item.id, (_) => item, customStore);
  }

  async clearLastLoadedItems(): Promise<void> {
    await clear(customStore);
  }

  async getLastLoadedItems(): Promise<IDCCFile[]> {
    return ((await values(customStore)) ?? []).map((x) => new FSFile(x));
  }

  async removeLastLoadedItem(item: IDCCFile): Promise<void> {
    await del(item.id, customStore);
  }

  performAutoSave(_item: IDCCFile, _data: string): Promise<void> {
    return Promise.resolve(undefined);
  }

  async promptLoad(): Promise<IDCCFile> {
    // @ts-ignore
    const [handle] = await window.showOpenFilePicker({
      multiple: false,
      ...types,
    });
    return await FSFile.fromHandle(handle);
  }

  async promptSaveAs(item: IDCCFile, data: string): Promise<IDCCFile> {
    return await this.saveNewItem(item.fileName, data);
  }

  async saveNewItem(newFileName: string, data: string): Promise<IDCCFile> {
    // @ts-ignore
    const handle = await window.showSaveFilePicker({
      suggestedName: newFileName,
      ...types,
    });
    const fsFile = await FSFile.fromHandle(handle);
    await fsFile.save(data);
    return fsFile;
  }
}
