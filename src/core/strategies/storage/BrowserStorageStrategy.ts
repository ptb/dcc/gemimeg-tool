import { IDCCFile, IStorageStrategy } from "./IStorageStrategy";
import { isXml, saveFile, timeout } from "~/assets/util/general";

interface LocalDCCFile {
  fileName: string;
  xml: string;
  dateTime: string;
  autoSave: boolean;
}

class BrowserDCCStorage {
  private readonly baseKey: string;

  constructor(baseKey = "gemimeg.tool") {
    this.baseKey = baseKey;
  }

  getAll(): Map<string, LocalDCCFile> {
    const savedDCCs = localStorage.getItem(`${this.baseKey}.storage`);
    if (savedDCCs) return new Map(JSON.parse(savedDCCs));
    return new Map();
  }

  saveAll(map: Map<string, LocalDCCFile>) {
    localStorage.setItem(
      `${this.baseKey}.storage`,
      JSON.stringify(Array.from(map.entries()))
    );
  }

  save(data: LocalDCCFile) {
    const savedDCCs = this.getAll();
    savedDCCs.set(data.fileName, data);
    this.saveAll(savedDCCs);
  }

  remove(key: string) {
    const savedDCCs = this.getAll();
    savedDCCs.delete(key);
    this.saveAll(savedDCCs);
  }
}

export class BrowserDCCFile implements IDCCFile {
  id: string;
  fileName: string;
  filePath: string;
  private readonly rawXml: string;
  dateTime: string;
  autoSave: boolean;
  private browserStorage: BrowserDCCStorage;

  constructor(
    fileName: string,
    rawXml: string,
    dateTime: string,
    autoSave: boolean,
    browserStorage: BrowserDCCStorage
  ) {
    this.fileName = fileName;
    this.filePath = fileName;
    this.id = fileName;
    this.rawXml = rawXml;
    this.dateTime = dateTime;
    this.autoSave = autoSave;
    this.browserStorage = browserStorage;
  }

  load(): Promise<string> {
    return Promise.resolve(this.rawXml);
  }

  delete(): Promise<void> {
    this.browserStorage.remove(this.fileName);
    return Promise.resolve();
  }

  save(data: string): Promise<void> {
    this.browserStorage.save({
      fileName: this.fileName,
      xml: data,
      dateTime: this.dateTime,
      autoSave: this.autoSave,
    });
    saveFile(this.fileName, data, "application/xml");
    return Promise.resolve();
  }
}

export class BrowserStorageStrategy implements IStorageStrategy {
  browserStorage: BrowserDCCStorage;

  constructor(baseKey?: string) {
    this.browserStorage = new BrowserDCCStorage(baseKey);
  }

  promptLoad(): Promise<IDCCFile> {
    return new Promise((resolve) => {
      let lock = false;
      // create invisible file input element
      const hiddenElement = document.createElement("input");
      hiddenElement.setAttribute("type", "file");

      hiddenElement.addEventListener(
        "change",
        (e) => {
          lock = true;
          const target = e.target as HTMLInputElement;
          // check if only one file has been selected
          if (target.files.length !== 1) {
            resolve(null);
            return;
          }

          const file = target.files[0];
          // check that the file type is xml
          if (!isXml(file.type)) {
            resolve(null);
            return;
          }

          // read the file contents with the FileReader
          const reader = new FileReader();
          reader.addEventListener("load", (res) => {
            const xml = res.target.result;
            hiddenElement.remove();
            resolve(
              new BrowserDCCFile(
                file.name,
                xml.toString(),
                new Date().toISOString(),
                false,
                this.browserStorage
              )
            );
          });
          reader.readAsText(file);
        },
        { once: true }
      );
      // https://stackoverflow.com/a/63773257
      // input type="file" has no event if it was cancelled
      // by using a focus event on the window and a timeout we can run code
      // after the prompt was shown and resolve with null if the lock is not
      // set and no file has been selected.
      window.addEventListener(
        "focus",
        async () => {
          await timeout(300);
          if (!lock) {
            resolve(null);
            hiddenElement.remove();
          }
        },
        { once: true }
      );
      hiddenElement.click();
    });
  }

  promptSaveAs(_file: IDCCFile, _data: string): Promise<IDCCFile> {
    throw new Error("Not implemented.");
  }

  saveNewItem(newFileName: string, data: string): Promise<IDCCFile> {
    const date = new Date();
    // save to browser storage
    this.browserStorage.save({
      fileName: newFileName,
      xml: data,
      dateTime: date.toISOString(),
      autoSave: true,
    });
    // save file to the device of the user
    saveFile(newFileName, data, "application/xml");

    return Promise.resolve(
      new BrowserDCCFile(
        newFileName,
        data,
        date.toISOString(),
        true,
        this.browserStorage
      )
    );
  }

  getLastLoadedItems(): Promise<IDCCFile[]> {
    return new Promise<IDCCFile[]>((resolve) => {
      resolve(
        Array.from(this.browserStorage.getAll().values()).map(
          (x) =>
            new BrowserDCCFile(
              x.fileName,
              x.xml,
              x.dateTime,
              x.autoSave,
              this.browserStorage
            )
        )
      );
    });
  }

  async addLastLoadedItem(item: IDCCFile): Promise<void> {
    this.browserStorage.save({
      fileName: item.fileName,
      xml: await item.load(),
      dateTime: item.dateTime,
      autoSave: item.autoSave,
    });
  }

  clearLastLoadedItems(): Promise<void> {
    this.browserStorage.saveAll(new Map<string, LocalDCCFile>());
    return Promise.resolve();
  }

  removeLastLoadedItem(item: IDCCFile): Promise<void> {
    this.browserStorage.remove(item.fileName);
    return Promise.resolve();
  }

  performAutoSave(item: IDCCFile, data: string): Promise<void> {
    // TODO: maybe unwanted behaviour to override actual saved file
    this.browserStorage.save({
      fileName: item.fileName,
      xml: data,
      dateTime: item.dateTime,
      autoSave: true,
    });
    return Promise.resolve();
  }
}
