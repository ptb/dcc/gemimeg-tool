export interface ISaveable<T> {
  /**
   * Saves the given data
   * @param data the data to save
   */
  save(data: T): Promise<void>;
}
