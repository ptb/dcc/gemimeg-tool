export interface ILoadable<T> {
  /**
   * Loads the data
   * @returns the loaded data
   */
  load(): Promise<T>;
}
