import { ILoadable } from "./interfaces/ILoadable";
import { ISaveable } from "./interfaces/ISaveable";

export interface IDCCFile extends ILoadable<string>, ISaveable<string> {
  id: string;
  fileName: string;
  filePath: string;
  dateTime: string;
  autoSave: boolean;
}

export interface IStorageStrategy {
  /**
   * Prompts the user to choose a file and loads it
   * @returns the file chosen by the user or null if none was selected
   */
  promptLoad(): Promise<IDCCFile>;

  /**
   * Prompts the user to select a location to save the file to
   * @param item the file to save
   * @param data the current file contents
   * @returns the IDCCFile for the new item
   */
  promptSaveAs(item: IDCCFile, data: string): Promise<IDCCFile>;

  /**
   * Saves the data into a new file and returns a new IDCCFile
   * @param newFileName fileName suggested to the user
   * @param data the data to save
   * @returns the IDCCFile for the new item or null if saving was aborted
   */
  saveNewItem(newFileName: string, data: string): Promise<IDCCFile>;

  /**
   * Get the last loaded items
   * @returns an array that contains the last loaded items
   */
  getLastLoadedItems(): Promise<IDCCFile[]>;

  /**
   * Removes all last loaded items
   */
  clearLastLoadedItems(): Promise<void>;

  /**
   * Adds an item to the last loaded items
   * @param item item to add
   */
  addLastLoadedItem(item: IDCCFile): Promise<void>;

  /**
   * Removes an item from the last loaded items
   * @param item item to remove
   */
  removeLastLoadedItem(item: IDCCFile): Promise<void>;

  /**
   * Preforms an auto save for the given item
   * @param item item to save
   * @param data data to save
   */
  performAutoSave(item: IDCCFile, data: string): Promise<void>;
}
