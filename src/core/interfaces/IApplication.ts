import { IPreviewStrategy } from "../strategies/preview/IPreviewStrategy";
import { IStorageStrategy } from "../strategies/storage/IStorageStrategy";
import { IValidationStrategy } from "../strategies/validation/IValidationStrategy";
import { ICredit } from "~/core/interfaces/ICredit";

export interface IApplication {
  previewStrategy: IPreviewStrategy;
  validationStrategy: IValidationStrategy;
  storageStrategy: IStorageStrategy;

  /**
   * Attaches an event listener
   * @param event event to attach to
   * @param callback callback to call
   */
  on(event: string, callback: Function): void;

  /**
   * Detaches an event listener
   * @param event event to detach
   * @param callback callback to detach
   */
  off(event: string, callback: Function): void;

  /**
   * Emits an event
   * @param event event to emit
   * @param args arguments to emit
   */
  emit(event: string, ...args: any[]): void;

  /**
   * Registers one or more credits
   * @param credits credits to register
   */
  registerCredits(credits: ICredit | ICredit[]);

  /**
   * Unregisters one or more credits
   * @param credits credits to unregister
   */
  unregisterCredits(credits: ICredit | ICredit[]);

  /**
   * List of registered credits
   */
  getCredits(): ICredit[];
}
