export interface ICredit {
  name: string;
  version: string;
  license: string;
  authors: string;
  url: string;
}
