import { ConditionType, DataType, ResultType } from "@d-ptb/dcc-js";

export function generateResult() {
  return new ResultType({
    data: new DataType(),
  });
}

export function generateCondition() {
  return new ConditionType({
    data: new DataType({
      quantity: [],
    }),
  });
}
