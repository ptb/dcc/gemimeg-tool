export const DECIMAL_REGEX =
  /^[-+]?((\d*\.\d+)|(\d+\.\d*)|(\d+\.?))([Ee][-+]?\d+)?$/;
export const POSITIVE_DECIMAL_REGEX =
  /^((\+?((\d*\.\d+)|(\d+\.\d*)|(\d+\.?))([Ee][-+]?\d+)?)\s?)*$/;
export const DECIMAL_LIST_REGEX =
  /^([-+]?((\d*\.\d+)|(\d+\.\d*)|(\d+\.?))([Ee][-+]?\d+)?)( ([-+]?((\d*\.\d+)|(\d+\.\d*)|(\d+\.?))([Ee][-+]?\d+)?))*$/;
export const BETWEEN_ZERO_AND_ONE_INCLUSIVE_REGEX =
  /^\+?((0\.\d*[1-9]\d*)|(\.\d*[1-9]\d*)|(1\.0*)|(1)|(0))$/;
export const GREATER_OR_EQUAL_ONE_REGEX = /^\+?(([1-9]\d*\.\d*)|([1-9]\d*))$/;
export const DATE_TIME_REGEX =
  /^([0-9]{4})-([0-1][0-9])-([0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](Z)?)$/;
export const DATE_TIME_LIST_REGEX =
  /^(([0-9]{4})-([0-1][0-9])-([0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](Z)?))( ([0-9]{4})-([0-1][0-9])-([0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9](Z)?))*$/;

export const BOOLEAN_LIST_REGEX = /^(true|false)( (true|false))*$/;

export const NOT_EMPTY_STRING_TYPE_REGEX = /^[^\s]+(\s+[^\s]+)*$/;

export const DSI_UNIT_REGEX =
  /^((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+(\\per((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+)?$|(^\|.*$)$/;
export const DSI_UNIT_LIST_REGEX =
  /^(((((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+(\\per((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+)?)|(\|\S*))(\s((((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+(\\per((((\\kilogram)|(\\decibel)|(\\degreecelsius)|(\\mmhg)|(\\minute)|(\\hour)|(\\day))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\gram))(\\tothe\{-?\d+(\.\d+)?\})?)|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\bel))(\\tothe\{-?\d+(\.\d+)?\})?)|((\\one))|(((\\deca)|(\\hecto)|(\\kilo)|(\\mega)|(\\giga)|(\\tera)|(\\peta)|(\\exa)|(\\zetta)|(\\yotta)|(\\deci)|(\\centi)|(\\milli)|(\\micro)|(\\nano)|(\\pico)|(\\femto)|(\\atto)|(\\zepto)|(\\yocto)|(\\kibi)|(\\mebi)|(\\gibi)|(\\tebi)|(\\pebi)|(\\exbi)|(\\zebi)|(\\yobi))?((\\metre)|(\\second)|(\\ampere)|(\\kelvin)|(\\mole)|(\\candela)|(\\degree)|(\\arcminute)|(\\arcsecond)|(\\radian)|(\\steradian)|(\\hertz)|(\\newton)|(\\pascal)|(\\joule)|(\\watt)|(\\coulomb)|(\\volt)|(\\farad)|(\\ohm)|(\\siemens)|(\\weber)|(\\tesla)|(\\henry)|(\\lumen)|(\\lux)|(\\becquerel)|(\\sievert)|(\\gray)|(\\katal)|(\\hectare)|(\\litre)|(\\tonne)|(\\electronvolt)|(\\dalton)|(\\astronomicalunit)|(\\neper)|(\\percent)|(\\ppm)|(\\byte)|(\\bit)|(\\angstrom)|(\\bar)|(\\atomicunittime)|(\\atomicmassunit)|(\\barn)|(\\clight)|(\\electronmass)|(\\elementarycharge)|(\\naturalunittime)|(\\hartree)|(\\bohr)|(\\nauticalmile)|(\\knot)|(\\planckbar))(\\tothe\{-?\d+(\.\d+)?\})?))+)?)|(\|\S*)))*)$/;
export const latestSchemaVersion = "3.2.1";
export const latestSchemaLocation = `https://ptb.de/dcc https://ptb.de/dcc/v${latestSchemaVersion}/dcc.xsd`;
// Ascending! list of supported DCC versions
export const supportedSchemaVersions = [
  "3.0.0",
  "3.1.0",
  "3.1.1",
  "3.1.2",
  "3.2.0",
  latestSchemaVersion,
];

// Wiki URIs
export const wikiURIs = {
  base: "https://dccwiki.ptb.de/",
  dccSoftware: "dccdccSoftware",
  coreData: "dcccoredata",
  coreDataCountryCode: "dcccoredata#countryCodeISO3166_1",
  coreDataLangCode: "dcccoredata#langCodeISO639_1",
  coreDataMandatoryLang: "dcccoredata#mandatoryLangISO639_1",
  coreDataUniqueIdentifier: "dcccoredata#uniqueIdentifier",
  coreDataReceiptDate: "dcccoredata#receiptDate",
  coreDataIdentifications: "dcccoredata#identifications",
  coreDataBeginPerformanceDate: "dcccoredata#beginPerformanceDate",
  coreDataEndPerformanceDate: "dcccoredata#endPerformanceDate",
  coreDataPerformanceLocation: "dcccoredata#performanceLocation",
  identifications: "dccidentificationType",
  identificationsIssuer: "dccidentificationType#issuer",
  identificationsValue: "dccidentificationType#value",
  identificationsName: "dccidentificationType#description",
  respPerson: "dccrespPerson",
  customer: "dcccustomer",
  location: "dcclocationType",
  calibrationLaboratory: "dcccalibrationLaboratory",
  richContent: "dccrichContentType",
  owner: "dccitems#owner",
  item: "dccitem",
  equipmentClass: "dccequipmentClassType",
  manufacturer: "dccitem",
  statement: "dccstatement",
  respAuthority: "dcccontactType",
  conformity: "dccstringConformityStatementStatusType",
  quantity: "dccquantityType",
  measurementResult: "dccmeasurementResult",
  usedMethods: "dccusedMethodType",
  measuringEquipment: "dccmeasuringEquipmentType",
  influenceConditions: "dccinfluenceConditionType",
  results: "resultListType",
  contact: "dcccontactType",
  certificate: "dcccertificate",
  dsi: "dccquantityType#dsi",
};
