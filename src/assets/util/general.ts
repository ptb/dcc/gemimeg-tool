import Vue from "vue";
import { cloneDeep } from "lodash";
import { wikiURIs } from "~/assets/util/constants";

export function removeFromArray<T>(arr: T[], ...items: T[]) {
  for (const item of items) {
    const index = arr.indexOf(item);
    if (index !== -1) {
      arr.splice(index, 1);
    }
  }
}

export function swap(arr: [], x: number, y: number) {
  const origin = arr[x];
  arr[x] = arr[y];
  Vue.set(arr, y, origin);
}

export function moveToLastItem<T>(arr: T[], item: T) {
  arr.push(arr.splice(arr.indexOf(item), 1)[0]);
}

export function isFunction(obj: any): boolean {
  return !!(obj && obj.constructor && obj.call && obj.apply);
}

export function firstOrDefault<T>(arr: T[], def: T = null) {
  if (Array.isArray(arr) && arr.length > 0) {
    return arr[0];
  }
  return def;
}

export function insertIf<T>(condition: boolean, ...elements: T[]) {
  return condition ? elements : [];
}

// method to clone an item, could lead to problems...
// maybe prototype is not cloned correctly?
export function cloneWithPrototype<T>(item: T): T {
  return cloneDeep(item);
}

/**
 * Returns a boolean whether a given mime type is xml.
 * @param mimeType the given mime type
 */
export function isXml(mimeType: string): boolean {
  return mimeType === "text/xml" || mimeType === "application/xml";
}

/**
 * Saves a file to the device of the user
 * @param fileName name of the file
 * @param contents contents of the file
 * @param mimeType mimeType of the file
 */
export function saveFile(
  fileName: string,
  contents: string,
  mimeType: string
): void {
  const hiddenElement = document.createElement("a");
  hiddenElement.href = `data:${mimeType};charset=utf-8,${encodeURIComponent(
    contents
  )}`;
  hiddenElement.target = "_blank";
  hiddenElement.download = fileName;
  hiddenElement.click();
  hiddenElement.remove();
}

/**
 * Resolves a promise after ms milliseconds
 * @param ms milliseconds to timeout
 */
export function timeout(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function loadFromUrl(url: string): Promise<string> {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          resolve(xhr.responseText);
        } else {
          reject(xhr.statusText);
        }
      }
    };
    xhr.send(null);
  });
}

export function getFileIcon(name) {
  if (name.includes(".doc")) {
    return "mdi-file-word";
  } else if (name.includes(".xls")) {
    return "mdi-file-excel";
  } else if (name.includes(".pdf")) {
    return "mdi-file-pdf-box";
  } else if (name.includes(".txt") || name.includes(".rtf")) {
    return "mdi-file-document";
  } else if (name.includes(".xml")) {
    return "mdi-file-xml-box";
  } else if (name.includes(".csv")) {
    return "mdi-file-table";
  } else if (
    name.includes(".js") ||
    name.includes(".py") ||
    name.includes(".ts") ||
    name.includes(".html")
  ) {
    return "mdi-file-code";
  } else if (name.includes(".zip") || name.includes(".rar")) {
    return "mdi-zip-box";
  } else {
    return "mdi-file";
  }
}

// replaces all dataBase64 Strings in a DCC XML DOM with dummy strings
// returns an array of the actual dataBase64 Strings
export function replaceB64ContentInDOM(node) {
  const b64Strings = [];
  helper(node);
  function helper(node) {
    if (node.tagName === "dcc:dataBase64") {
      b64Strings.push(node.innerHTML);
      node.innerHTML = "";
      node.text = "";
    }
    node = node.firstChild;
    while (node) {
      helper(node);
      node = node.nextSibling;
    }
  }
  return b64Strings;
}

export function downloadFile(item, fileName) {
  const link = document.createElement("a");
  link.href = item;
  link.target = "_blank";
  link.download = fileName;
  link.dispatchEvent(new MouseEvent("click"));
}

export function isQuotaExceeded(e) {
  let quotaExceeded = false;
  if (e) {
    if (e.code) {
      switch (e.code) {
        case 22:
          quotaExceeded = true;
          break;
        case 1014:
          // Firefox
          if (e.name === "NS_ERROR_DOM_QUOTA_REACHED") {
            quotaExceeded = true;
          }
          break;
      }
    } else if (e.number === -2147024882) {
      // Internet Explorer 8
      quotaExceeded = true;
    }
  }
  return quotaExceeded;
}

export function generateWikiUri(element) {
  return wikiURIs.base + "en/" + wikiURIs[element];
}
