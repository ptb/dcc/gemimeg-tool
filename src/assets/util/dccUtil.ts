import {
  DCCDocument,
  DCCXMLElement,
  SoftwareType,
  TextType,
  StringWithLangType,
  ensureArray,
} from "@d-ptb/dcc-js";
import { latestSchemaVersion } from "~/assets/util/constants";

export function createEmptyDcc() {
  const dccDocument = DCCDocument.createEmpty();

  dccDocument.digitalCalibrationCertificate._attr.schemaVersion =
    latestSchemaVersion;

  // needed if the dccDocument is created without xml
  dccDocument.digitalCalibrationCertificate.administrativeData.coreData.mandatoryLangCodeISO639_1.push(
    // new DCCXMLElement({ _text: "de" }),
    new DCCXMLElement({ _text: "en" })
  );
  dccDocument.digitalCalibrationCertificate.administrativeData.coreData.usedLangCodeISO639_1.push(
    // new DCCXMLElement({ _text: "de" }),
    new DCCXMLElement({ _text: "en" })
  );
  dccDocument.digitalCalibrationCertificate.administrativeData.dccSoftware.software.push(
    new SoftwareType({
      name: new TextType({
        content: [
          new StringWithLangType({
            _text: "GEMIMEG-Tool",
          }),
        ],
      }),
      release: new DCCXMLElement({ _text: `v${process.env.version}` }),
    })
  );

  return dccDocument;
}

export function getDisplayedUnitAndValue(item) {
  const values = [];
  const units = [];

  if (item.constant) {
    for (const constant of ensureArray(item.constant)) {
      values.push(constant.value._text);
      units.push(constant.unit._text);
    }
  } else if (item.real) {
    for (const real of ensureArray(item.real)) {
      values.push(real.value._text);
      units.push(real.unit._text);
    }
  } else if (item.hybrid) {
    for (const hybrid of ensureArray(item.hybrid)) {
      const displayedHybrid = getDisplayedUnitAndValue(hybrid);
      values.push(...displayedHybrid.values);
      units.push(...displayedHybrid.units);
    }
  } else if (item.realListXMLList) {
    for (const realListXMLList of ensureArray(item.realListXMLList)) {
      values.push(realListXMLList.valueXMLList._text);
      units.push(realListXMLList.unitXMLList._text);
    }
  } else {
    if (item.value) {
      values.push(item.value._text);
    } else if (item.valueXMLList) {
      values.push(item.valueXMLList._text);
    }

    if (item.unit) {
      units.push(item.unit._text);
    } else if (item.unitXMLList) {
      units.push(item.unitXMLList._text);
    }
  }
  if (values.length < 1) values.push("-");
  if (units.length < 1) units.push("-");

  return {
    values,
    units,
  };
}

/**
 * Removes all empty or whitespace elements from the content array in-place
 * @param content array to clean up
 */
export function cleanUpContentArray(content: StringWithLangType[]) {
  let i = 0;
  let length = content.length;
  while (i < length) {
    if (content[i]._text.trim() === "") {
      content.splice(i, 1);
      length--;
    } else {
      i++;
    }
  }
}
