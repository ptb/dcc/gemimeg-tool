function fallbackCopyToClipboard(str: string): Promise<boolean> {
  return new Promise((resolve) => {
    const el = document.createElement("textarea");
    el.value = str;
    el.setAttribute("readonly", "");
    el.style.position = "absolute";
    el.style.left = "-9999px";
    document.body.appendChild(el);
    el.focus();
    el.select();
    const successful = document.execCommand("copy");
    document.body.removeChild(el);
    resolve(successful);
  });
}

export async function copyToClipboard(str: string): Promise<void> {
  if (!navigator.clipboard) {
    await fallbackCopyToClipboard(str);
    return;
  }
  await navigator.clipboard.writeText(str);
}
