import * as Mathlive from "mathlive";
import VueMathfield from "mathlive/dist/vue-mathlive.mjs";
import Vue from "vue";

Vue.use(Mathlive, VueMathfield);
Vue.component("FormulaField", VueMathfield);
