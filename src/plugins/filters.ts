import Vue from "vue";
import { DCCXMLElement } from "@d-ptb/dcc-js";

Vue.filter("formatLocation", (location) => {
  if (!location) return "-";

  let locationString = "";

  if (location.street) locationString += location.street._text;
  if (location.streetNo) locationString += ` ${location.streetNo._text}`;
  if (location.postCode) locationString += `, ${location.postCode._text}`;
  if (location.city) locationString += ` ${location.city._text}`;

  return locationString;
});

Vue.filter("_textOrDashIfUndefined", (value: DCCXMLElement) => {
  return value ? value._text : "-";
});

Vue.filter("_joinArray", (value: [], separator = ", ") => {
  if (!Array.isArray(value)) return "-";
  return value.join(separator);
});

Vue.filter("_joinXmlArray", (value: DCCXMLElement[], separator = ", ") => {
  if (!Array.isArray(value)) return "-";
  return value.map((x) => x._text).join(separator);
});
