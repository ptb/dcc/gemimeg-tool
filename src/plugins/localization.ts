function setVuetifyLocale(vuetify, newLocale) {
  import(`vuetify/es5/locale/${newLocale}.js`).then((locale) => {
    vuetify.framework.lang.locales[newLocale] = locale.default;
    vuetify.framework.lang.current = newLocale;
  });
}

export default ({ app }) => {
  setVuetifyLocale(app.vuetify, app.i18n.locale);

  app.i18n.onLanguageSwitched = (_oldLocale, newLocale) => {
    setVuetifyLocale(app.vuetify, newLocale);
  };
};
