import { Toasted, ToastOptions } from "vue-toasted";
import { Logger } from "~/plugins/logger";

export class Notifier {
  private readonly logger: Logger;
  private toast: Toasted;

  constructor(logger: Logger, toast: Toasted) {
    this.logger = logger.withTag("Notifier");
    this.toast = toast;
  }

  private show(message: string, type: string, options: ToastOptions = {}) {
    this.logger[type](message);
    this.toast.show(message, {
      type,
      className: type === "warn" ? "warning" : "",
      action: {
        text: "Close",
        onClick: (_e, toastObject) => {
          toastObject.goAway(0);
        },
      },
      ...options,
    });
  }

  info(message: string, options: ToastOptions = {}) {
    this.show(message, "info", options);
  }

  success(message: string, options: ToastOptions = {}) {
    this.show(message, "success", options);
  }

  warn(message: string, options: ToastOptions = {}) {
    this.show(message, "warn", options);
  }

  error(message: string, options: ToastOptions = {}) {
    this.show(message, "error", options);
  }
}

export default ({ $logger, $toast }, inject) => {
  inject("notifier", new Notifier($logger, $toast));
};
