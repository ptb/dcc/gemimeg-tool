import { GemimegTool } from "~/core/GemimegTool";
import { XMLLintValidationStrategy } from "~/core/strategies/validation/XMLLintValidationStrategy";
import { BrowserStorageStrategy } from "~/core/strategies/storage/BrowserStorageStrategy";
import { FileSystemAPIStorageStrategy } from "~/core/strategies/storage/FileSystemAPIStorageStrategy";
import { IStorageStrategy } from "~/core/strategies/storage/IStorageStrategy";
import { SaxonJSPreviewStrategy } from "~/core/strategies/preview/saxon-js/SaxonJSPreviewStrategy";

export default ({ $global, $logger }, inject) => {
  let storageStrategy: IStorageStrategy;
  // check if FileSystem API is supported by the browser
  if ("showOpenFilePicker" in window) {
    $logger.info("Using FileSystem API");
    storageStrategy = new FileSystemAPIStorageStrategy();
  } else {
    $logger.info("Falling back to browser storage");
    storageStrategy = new BrowserStorageStrategy();
  }

  const tool = new GemimegTool($global);

  tool.storageStrategy = storageStrategy;

  tool.previewStrategy = new SaxonJSPreviewStrategy(
    tool,
    // @ts-ignore
    `${process.env.settings.base}xslt-dcc/dcc.xsl`,
    "xslt-dcc"
  );

  tool.validationStrategy = new XMLLintValidationStrategy();

  inject("tool", tool);
};
