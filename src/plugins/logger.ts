import consola, { BasicReporter, BrowserReporter, Consola } from "consola";

export type Logger = Consola;

export default ({ $config }, inject) => {
  let reporter;
  // @ts-ignore IE check
  if (window.document.documentMode) {
    reporter = new BasicReporter();
  } else {
    reporter = new BrowserReporter();
  }

  inject(
    "logger",
    consola.create({
      reporters: [reporter],
      level: $config.logLevel,
    })
  );
};
