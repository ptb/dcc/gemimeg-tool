import Vue from "vue";
import { DCCDocument } from "@d-ptb/dcc-js";
import { debounce } from "lodash";

export interface IGlobal extends Vue {
  loadingDcc: boolean;
  savingDcc: boolean;
  dccDocument: DCCDocument;
  getConfirmation(i18n: string): boolean;
  updatePreview(dccDocument: DCCDocument): void;
}

export default (_, inject) => {
  inject(
    "global",
    new Vue({
      data: {
        dccDocument: null,
        loadingDcc: false,
        savingDcc: false,
        hasUnsavedChanges: false,
      },
      watch: {
        dccDocument: {
          deep: true,
          handler: debounce(
            function () {
              if (!this.loadingDcc) {
                this.hasUnsavedChanges = true;
              }
            },
            1000,
            { leading: true }
          ),
        },
      },
      methods: {
        getConfirmation(i18n: string) {
          // @ts-ignore
          return this.$refs.confirmationDialog.getConfirmation(i18n);
        },
        updatePreview(dccDocument: DCCDocument) {
          this.$emit("updatePreview", dccDocument);
        },
      },
    })
  );
};
