import { generateConfig } from "./nuxt.config.generator";

export default {
  ...generateConfig(),
};
