<div align="center">
  <a href="https://gemimeg.de/" traget="_blank"><img src="src/assets/logo/gemimeg.png" alt="" width="600"/></a>
  <br />
  <h1>GEMIMEG-Tool</h1>
  <p>Example for the implementation of a GUI for the Digital Calibration Certificate</p>
  <h3>
    [Releases](https://gitlab.com/ptb/dcc/gemimeg-tool/-/releases) | [Developer Documentation](docs/README.md) | [Issues](https://gitlab.com/ptb/dcc/gemimeg-tool/-/issues)
  </h3>

  <h1>Warning!</h1>
  <h2>This tool is still work in progress. Some features aren't implemented yet, some don't work as intended.</h2>
</div>
<br />

## Getting started

You can try out the GEMIMEG-Tool online! Check out one of the links below.

| Branch  | Description                  | URL                                      |
|---------|------------------------------|------------------------------------------|
| master  | Latest stable release        | https://www.gemimeg.ptb.de/gemimeg-tool/ |
| develop | Unstable development variant | https://ptb.gitlab.io/dcc/gemimeg-tool/  |


### > Supports v3 of the DCC


## Development

### > [Have a look at the developer documentation](docs/README.md)


## Preview

![Preview of the Tool](docs/img/preview.png)


## License

Copyright 2021 Physikalisch-Technische Bundesanstalt, Bundesallee 100, 38116 Braunschweig, GERMANY
Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");

You may not use this work except in compliance with the Licence.

You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is
distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.

See [the Licence](LICENSE.md) for the specific language governing permissions and limitations under the Licence.

## Maintainer

<table>
  <tr>
    <th><a href="https://gitlab.com/loewePTB" target="_blank"><img width="200" src="https://gitlab.com/uploads/-/system/user/avatar/9414133/avatar.png"/></a></th>
    <th><a href="https://gitlab.com/jordanPTB" target="_blank"><img width="200" src="https://secure.gravatar.com/avatar/cefaf9e44816ba34f96939d7ba8d838a?s=800&d=identicon"/></a></th>
    <th><a href="https://gitlab.com/demirPTB" target="_blank"><img width="200" src="https://secure.gravatar.com/avatar/4f42ae8ed1bd489b8256f8b9e3d6b1d5?s=800&d=identicon"/></a></th>
  </tr>
  <tr>
    <td align="center"><a href="https://gitlab.com/loewePTB" target="_blank">Jan Loewe</a></td>
    <td align="center"><a href="https://gitlab.com/jordanPTB" target="_blank">Moritz Jordan</a></td>
    <td align="center"><a href="https://gitlab.com/demirPTB" target="_blank">Muhammed-Ali Demir</a></td>
  </tr>
</table>
