# PTB Proxy settings

When you want to work on this project inside the PTB, you'll need the correct proxy settings.
Otherwise, web requests will time out and the installation of e.g. dependencies will fail.


## General

In general, you should have the following two environment variables set.

```dotenv
HTTP_PROXY=http://webproxy.bs.ptb.de:8080/
HTTPS_PROXY=http://webproxy.bs.ptb.de:8080/
```


## git

Execute both commands below to configure the PTB proxy for git.

```sh
git config --global http.proxy http://webproxy.bs.ptb.de:8080/
git config --global https.proxy http://webproxy.bs.ptb.de:8080/
```


## npm

Execute both commands below to configure the PTB proxy for npm.

```sh
npm config set proxy http://webproxy.bs.ptb.de:8080/
npm config set https-proxy http://webproxy.bs.ptb.de:8080/
```

## yarn

Execute both commands below to configure the PTB proxy for yarn.

```sh
yarn config set -H httpProxy http://webproxy.bs.ptb.de:8080/
yarn config set -H httpsProxy http://webproxy.bs.ptb.de:8080/
```
