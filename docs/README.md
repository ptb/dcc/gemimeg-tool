# Developer Documentation


## Prerequisites

- Git
- Node 20 LTS
- [Yarn Berry](https://github.com/yarnpkg/berry) (using [Corepack](https://github.com/nodejs/corepack))


### > [Inside of the PTB you need the correct proxy settings!](proxy.md)
Please follow this link and make sure, that you have the correct settings.


## Getting started

1. First you need to clone the GEMIMEG-Tool repository

```sh
# It is important that you clone the repository with submodules!
git clone --recurse-submodules https://gitlab.com/ptb/dcc/gemimeg-tool.git
```

2. Open the project folder and download dependencies with yarn

```sh
# cd into the repository
cd gemimeg-tool

# enable corepack (only needed once) - Open a command prompt or terminal with admin rights for this step
corepack enable

# Install dependencies - If this does not work in the same command prompt or terminal as above try opening a second 
# command prompt or terminal without admin rights 
yarn

# If you have to install the oss-attribution-generator
npm i -g oss-attribution-generator

# Generate the credits.json (containing licensing information of used npm packages)
yarn run attribution:generate
```

3. Start coding!


## Building and running the docker image

> :warning: **If you use docker installed on Windows without iptables you might need to add the `--network=host` option.**

```sh
# without proxy
docker build -t gemimeg-tool .

# behind ptb proxy
docker build -t gemimeg-tool . --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy

# starting a container
docker run -it -p 80:3000 gemimeg-tool
```


## Predefined Scripts

### `yarn dev`

Run the project with development configuration.

### `yarn build`

Build the project for a production environment. Output will be in the `dist/` folder.
You can also use `yarn nuxt:generate` or `yarn nuxt:generate-relative` to build the tool.

### `yarn lint`

Lint the code with `eslint`.

### `yarn lint:fix`

Lint the code with `eslint` and fix problems automatically.

### `yarn attribution:generate`

Generate the credits.json (containing licensing information of used npm packages).


## Git

The master branch contains the latest stable version. All development is done in the develop branch.

This project is using the [Conventional Commit Standard](https://www.conventionalcommits.org/en/v1.0.0/).

Tags for versions look like this: `v1.2.3`

### `feat-xyz`

Branches for new features should start with `feat-` following the name of the feature.


### `fix-xyz`

Branches for bugfixes should start with `fix-` following the name of the bugfix.


## Frameworks used

- [Nuxt.js](https://nuxtjs.org/) (Framework for Vue with SSR and sensible defaults)
- [Vuetify](https://vuetifyjs.com/en/) (Framework for Material Design Components)
- Other Dependencies can be found in [package.json](../package.json)
